import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators,FormArray,FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  genders = ['male', 'female'];
  signupForm:FormGroup;
  forbiddenUsernames = ['Cris' , 'Anna'];

  ngOnInit() {
  	this.signupForm = new FormGroup({
      'userData' : new FormGroup({
    		'username': new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
    		'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails),
      }),
  		'gender': new FormControl('male'),
      'hobbies' : new FormArray([])
  	});
    /*
     * Very cool console logs anytime there is a value change in form
     */
    // this.signupForm.valueChanges.subscribe(
    //   (value)=>console.log(value)
    // );
    /*
     * Very cool console logs anytime there is a value change in form
     */
    this.signupForm.statusChanges.subscribe(
      (status)=>console.log(status)
    );
    /*
     * This is jsut like the template driven approach where you have to spefcify entire form object
     */
    this.signupForm.setValue({
      'userData': {
        'username':'Max',
        'email':'max@test.com',
      },
      'gender':'male',
      'hobbies': []
    });
    /*
     * This is jsut like the template driven approach where you have to spefcify entire form object
     */
    this.signupForm.patchValue({
      'userData': {
        'username':'Max',
        'email':'max@test.com',
      },
    });
  }
  onSubmit() {
    console.log(this.signupForm)
    this.signupForm.reset();
  }
  onAddHobby() {
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(control);
  }
  forbiddenNames(control: FormControl): {[s:string]:boolean} {
    if (this.forbiddenUsernames.indexOf(control.value) !== -1 ) {
      return {'nameIsForbidden':true};
    }
    return null;
  }

  forbiddenEmails(control:FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve,reject) => {
      setTimeout( ()=>{
        if (control.value === 'test@test.com') {
          resolve({'emailIsForbidden':true})
        }
      },1500);
    });
    return promise;
  }
}
